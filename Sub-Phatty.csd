<Cabbage> bounds(0, 0, 0, 0)

;- Region: GUI
//Aspect Ratios calculated from actual product dimensions
form caption("SUB PHATTY") size(1024, 621), colour(50, 50, 50), pluginid("def1")

//image bounds(22, 6, 971, 321) file("Assets\Sub_Phatty_Controls.PNG") alpha(0.3)
image bounds(10, 10, 1004, 330) file("Assets\metal-1.png") corners(50) shape("circle")

groupbox bounds(40, 19, 943, 23) outlinethickness(0)
keyboard bounds(291, 350, 702, 257), keywidth(47), scrollbars(1)//, middlec(7),  value(48)

//_________________________________________________________PRESETS____________________________________________________________
groupbox bounds(40, 19, 89, 292) colour(255, 255, 255, 0) outlinecolour(0, 0, 0)  text("PRESETS") linethickness(0)

groupbox bounds(91, 186, 27, 28) outlinethickness(0) colour(0, 0, 0, 125) // Shadow Effect
button bounds(91, 186, 26, 26) text("", "") colour:0(147, 147, 147, 255) colour:1(255, 151, 65, 255) corners(5)

groupbox bounds(52, 186, 27, 28) outlinethickness(0) colour(0, 0, 0, 125)
button bounds(52, 186, 26, 26) text("", "") colour:0(147, 147, 147, 255) colour:1(255, 151, 65, 255) corners(5)

groupbox bounds(91, 146, 27, 28) outlinethickness(0) colour(0, 0, 0, 125)
button bounds(91, 146, 26, 26) text("", "") colour:0(147, 147, 147, 255) colour:1(255, 151, 65, 255) corners(5)

groupbox bounds(52, 146, 27, 28) outlinethickness(0) colour(0, 0, 0, 125)
button bounds(52, 146, 26, 26) text("", "") colour:0(147, 147, 147, 255) colour:1(255, 151, 65, 255) corners(5)

groupbox bounds(91, 104, 27, 28) outlinethickness(0) colour(0, 0, 0, 125)
button bounds(91, 104, 26, 26) text("", "") colour:0(147, 147, 147, 255) colour:1(255, 151, 65, 255) corners(5)

groupbox bounds(52, 104, 27, 28) outlinethickness(0) colour(0, 0, 0, 125)
button bounds(52, 104, 26, 26) text("", "") colour:0(147, 147, 147, 255) colour:1(255, 151, 65, 255) corners(5)

groupbox bounds(91, 62, 27, 28) outlinethickness(0) colour(0, 0, 0, 125)
button bounds(91, 62, 26, 26)  text("", "") colour:0(147, 147, 147, 255) colour:1(255, 151, 65, 255) corners(5)

groupbox bounds(52, 62, 27, 28) outlinethickness(0) colour(0, 0, 0, 125)
button bounds(52, 62, 26, 26)  text("", "") colour:0(147, 147, 147, 255) colour:1(255, 151, 65, 255) corners(5)

groupbox bounds(71, 252, 27, 28) outlinethickness(0) colour(0, 0, 0, 125)
button bounds(71, 252, 26, 26) text("", "") colour:0(147, 147, 147, 255) colour:1(255, 151, 65, 255) corners(5)

//_________________________________________________________PITCH____________________________________________________________
groupbox bounds(128, 19, 78, 292) colour(255, 255, 255, 0) outlinecolour(0, 0, 0)  text("PITCH") linethickness(0)
label bounds(138, 56, 64, 10) text("FINE TUNE") fontcolour(0, 0, 0, 255)
rslider bounds(150, 66, 40, 40) range(-1, 1, 0, 1, 0.01)  channel("Fine_Tune")  imgfile("Slider", "Assets/Knob.png")  
label bounds(138, 140, 64, 10) text("GLIDE RATE") fontcolour(0, 0, 0, 255)
rslider bounds(150, 150, 40, 40) range(0, 10, 5, 1, 1) channel("Glide_Rate") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png") 
groupbox bounds(172, 252, 27, 28) outlinethickness(0) colour(0, 0, 0, 125)
button bounds(172, 252, 26, 26) text("", "") colour:0(147, 147, 147, 255) colour:1(255, 151, 65, 255) corners(5) channel("Octave_Up")
groupbox bounds(136, 252, 27, 28) outlinethickness(0) colour(0, 0, 0, 125)
button bounds(136, 252, 26, 26) text("", "") colour:0(147, 147, 147, 255) colour:1(255, 151, 65, 255) corners(5) channel("Octave_Down")

//_________________________________________________________MODULATION____________________________________________________________
groupbox bounds(205, 19, 136, 292) colour(255, 255, 255, 0) outlinecolour(0, 0, 0, 255)  text("MODULATION") linethickness(0)

label bounds(208, 56, 64, 10) text("LFO RATE") fontcolour(0, 0, 0, 255)
rslider bounds(220, 66, 40, 40) range(0.1, 100, 5, 1, 0.1) channel("LFO_Rate") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png") 
label bounds(276, 56, 64, 10) text("SOURCE") fontcolour(0, 0, 0, 255)
rslider bounds(288, 66, 40, 40) range(1, 6, 6, 1, 1) channel("Source") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png") 
label bounds(208, 140, 64, 10) text("PITCH AMT") fontcolour(0, 0, 0, 255)
rslider bounds(220, 150, 40, 40) range(0, 1, 1, 1, 0.001) channel("Pitch_AMT") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png") 
groupbox bounds(292, 168, 27, 28) outlinethickness(0) colour(0, 0, 0, 125)
label bounds(275, 155, 64, 10) text("PITCH AMT") fontcolour(0, 0, 0, 255)
button bounds(292, 168, 26, 26) text("", "") colour:0(147, 147, 147, 255) colour:1(255, 151, 65, 255) corners(5) channel("OSC_2_Only")
label bounds(273, 200, 64, 10) text("OSC 2 ONLY") fontcolour(0, 0, 0, 255)
label bounds(206, 226, 64, 10) text("FILTER AMT") fontcolour(0, 0, 0, 255)
rslider bounds(218, 236, 40, 40) range(0, 1, 1, 1, 0.001) channel("Filter_AMT") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png") 
label bounds(272, 226, 64, 10) text("WAVE AMT") fontcolour(0, 0, 0, 255)
rslider bounds(284, 236, 40, 40) range(0, 1, 1, 1, 0.001) channel("Wave_AMT") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png") 

//_________________________________________________________OSCILLATORS____________________________________________________________
groupbox bounds(340, 19, 131, 292) colour(35, 35, 35, 0) text("OSCILLATORS") outlinecolour(0, 0, 0, 255) linethickness(0)

//_________________________________________________________OSC1___________________________________________________________________
groupbox bounds(340, 39, 65, 272) colour(255, 255, 255, 0)  fontcolour(0, 0, 0, 255) outlinecolour(0, 0, 0) align("left") text("1") linethickness(0)
label bounds(338, 62, 64, 10) text("OCTAVE") fontcolour(0, 0, 0, 255)
rslider bounds(350, 72, 40, 40) range(0, 3, 1, 1, 1)  channel("OSC1_Octave") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")
groupbox bounds(360, 168, 27, 28) outlinethickness(0) colour(0, 0, 0, 125)
label bounds(341, 155, 64, 10) text("HARD SYNC") fontcolour(0, 0, 0, 255)
button bounds(360, 168, 26, 26) text("", "") colour:0(147, 147, 147, 255) colour:1(255, 151, 65, 255) popuptext("Hard Sync") corners(5) channel("Hard_Sync")
label bounds(341, 200, 64, 10) text("OSC 2") fontcolour(0, 0, 0, 255)
label bounds(338, 226, 64, 10) text("WAVE") fontcolour(0, 0, 0, 255)
rslider bounds(350, 236, 40, 40) range(0, 3, 1, 1, 0.1) channel("OSC1_Wave") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")

//_________________________________________________________OSC2___________________________________________________________________
groupbox bounds(404, 39, 67, 272) colour(255, 255, 255, 0)  fontcolour(0, 0, 0, 255) outlinecolour(0, 0, 0) align("left") text("2") linethickness(0)
label bounds(404, 62, 64, 10) text("OCTAVE") fontcolour(0, 0, 0, 255)
rslider bounds(416, 72, 40, 40)  range(0, 3, 1, 1, 1) channel("OSC2_Octave") imgfile("Slider", "Assets/Knob.png")
label bounds(404, 142, 64, 10) text("FREQUENCY") fontcolour(0, 0, 0, 255)
rslider bounds(416, 152, 40, 40) range(-7, 7, 0, 1, 1) channel("OSC2_Frequency") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")
label bounds(404, 226, 64, 10) text("WAVE") fontcolour(0, 0, 0, 255)
rslider bounds(416, 236, 40, 40) range(0, 3, 1, 1, 0.1)  channel("OSC2_Wave") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")

//_________________________________________________________MIXER__________________________________________________________________
groupbox bounds(470, 19, 83, 292) colour(35, 35, 35, 0) text("MIXER") outlinecolour(0, 0, 0) linethickness(0)
label bounds(466, 44, 64, 10) text("OSC 1") fontcolour(0, 0, 0, 255)
rslider bounds(478, 54, 40, 40) range(0, 1, 0.5, 1, 0.001)  channel("OSC1_Amp")  imgfile("Slider", "Assets/Knob.png")
label bounds(486, 108, 64, 10) text("SUB OSC") fontcolour(0, 0, 0, 255)
rslider bounds(498, 118, 40, 40) range(0, 1, 0, 1, 0.001)  channel("SUB_Amp") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")
label bounds(466, 172, 64, 10) text("OSC 2") fontcolour(0, 0, 0, 255)
rslider bounds(478, 182, 40, 40) range(0, 1, 0.5, 1, 0.001)  channel("OSC2_Amp") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")
label bounds(486, 242, 64, 10) text("NOISE") fontcolour(0, 0, 0, 255)
rslider bounds(498, 252, 40, 40) range(0, 1, 0, 1, 0.001)  channel("Noise_Amp") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")

//_________________________________________________________FILTER__________________________________________________________________
groupbox bounds(552, 19, 127, 292) colour(35, 35, 35, 0) text("FILTER")  outlinecolour(0, 0, 0) linethickness(0) 
label bounds(577, 50, 64, 10) text("CUTOFF") fontcolour(0, 0, 0, 255)
rslider bounds(580, 60, 56, 56) range(20, 20000, 4400, 1, 20)  channel("Cutoff") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")
label bounds(552, 144, 64, 9) text("RESONANCE") fontcolour(0, 0, 0, 255)
rslider bounds(566, 154, 40, 40) range(0, 0.9, 0.3, 1, 0.001)  channel("Resonance") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")
label bounds(614, 144, 64, 9) text("MULTIDRIVE") fontcolour(0, 0, 0, 255)
rslider bounds(624, 154, 40, 40) range(0, 1, 0, 1, 0.001)  channel("Multidrive") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")
label bounds(552, 226, 64, 9) text("EG AMOUNT") fontcolour(0, 0, 0, 255)
rslider bounds(566, 236, 40, 40) range(-5, 5, 0, 1, 1)  channel("EG") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")
label bounds(614, 226, 64, 9) text("KB AMOUNT") fontcolour(0, 0, 0, 255)
rslider bounds(624, 236, 40, 40) range(0, 10, 0, 1, 0.5)  channel("KB") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")

//_________________________________________________________ENVELOPE__________________________________________________________________
groupbox bounds(678, 19, 238, 292) colour(35, 35, 35, 0) text("ENVELOPE") outlinecolour(0, 0, 0) linethickness(0)

//_________________________________________________________FILTER_ENVELOPE__________________________________________________________________
groupbox bounds(678, 125, 238, 93) colour(35, 35, 35, 0) text("FILTER")fontcolour(0, 0, 0, 255) outlinecolour(0, 0, 0)  outlinethickness(0)// linethickness(2)
groupbox bounds(683, 125, 227, 2)
label bounds(676, 55, 64, 10) text("ATTACK") fontcolour(0, 0, 0, 255)
rslider bounds(690, 70, 40, 40) range(0.001, 10, 0.1, 1, 0.001) channel("ENV_Attack") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")
label bounds(730, 55, 64, 10) text("DECAY") fontcolour(0, 0, 0, 255)
rslider bounds(746, 70, 40, 40) range(0.001, 10, 0.1, 1, 0.001) channel("ENV_Decay") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")
label bounds(788, 55, 64, 10) text("SUSTAIN") fontcolour(0, 0, 0, 255)
rslider bounds(802, 70, 40, 40) range(0.001, 10, 5, 1, 0.001) channel("ENV_Sustain") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")
label bounds(850, 55, 64, 10) text("RELEASE") fontcolour(0, 0, 0, 255)
rslider bounds(862, 70, 40, 40) range(0.001, 10, 0.51, 1, 0.001) channel("ENV_Release") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")

//_________________________________________________________AMPLIFIER_ENVELOPE__________________________________________________________________
groupbox bounds(678, 162, 238, 93) colour(35, 35, 35, 0) text("AMPLIFIER")fontcolour(0, 0, 0, 255) outlinecolour(0, 0, 0) outlinethickness(0)// linethickness(2)
groupbox bounds(683, 180, 227, 2)
label bounds(676, 238, 64, 10) text("ATTACK") fontcolour(0, 0, 0, 255)
rslider bounds(690, 186, 40, 40) range(0.001, 10, 0.1, 1, 0.001) channel("AMP_Attack") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")
label bounds(730, 238, 64, 10) text("DECAY") fontcolour(0, 0, 0, 255)
rslider bounds(744, 186, 40, 40) range(0.001, 10, 0.1, 1, 0.001) channel("AMP_Decay") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")
label bounds(788, 238, 64, 10) text("SUSTAIN") fontcolour(0, 0, 0, 255)
rslider bounds(802, 186, 40, 40) range(0.001, 10, 5, 1, 0.001) channel("AMP_Sustain") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")
label bounds(850, 238, 64, 10) text("RELEASE") fontcolour(0, 0, 0, 255)
rslider bounds(860, 186, 40, 40) range(0.001, 10, 0.5, 1, 0.001) channel("AMP_Release") textcolour(0, 0, 0, 255) imgfile("Slider", "Assets/Knob.png")

//_________________________________________________________OUTPUT__________________________________________________________________
groupbox bounds(915, 19, 68, 292) colour(35, 35, 35, 0) text("OUTPUT") outlinecolour(0, 0, 0) linethickness(0) 
label bounds(914, 58, 64, 9) text("MASTER") fontcolour(0, 0, 0, 255)
rslider bounds(926, 68, 40, 40) range(0, 1, 0.2, 1, 0.001)  channel("Master")  imgfile("Slider", "Assets/Knob.png")
label bounds(914, 138, 64, 9) text("HEADPHONE") fontcolour(0, 0, 0, 255)
rslider bounds(926, 148, 40, 40) range(0, 1, 0, 1, 0.001)  channel("Headphone") imgfile("Slider", "Assets/Knob.png")

//image bounds(0, 0, 1024, 621) file("Assets\Sub_Phatty_Template.png")


vslider bounds(44, 400, 56, 166) range(-2, 2, 0, 1, 0.001) channel("Pitch_Bend") trackercolour(255, 255, 255, 255)
vslider bounds(152, 400, 56, 165) range(0, 1, 0.5, 1, 0.001) channel("Mod_Wheel") trackercolour(255, 255, 255, 255)

keyboard bounds(483, 165, 400, 100)

//groupbox bounds(40, 38, 943, 271) colour(0, 0, 0, 0) linethickness(0)  outlinethickness(8) outlinecolour(0, 0, 0)



</Cabbage>
</Cabbage>
<CsoundSynthesizer>
<CsOptions>
-n -d -+rtmidi=NULL -M0 -m0d --midi-key-cps=4 --midi-velocity-amp=5
</CsOptions>
<CsInstruments>
; Initialize the global variables. 
ksmps = 32
nchnls = 2
0dbfs = 1


gifn ftgen	0,0, 257, 9, .5,1,270,1.5,.33,90,2.5,.2,270,3.5,.143,90,4.5,.111,270 // http://www.csounds.com/manual/html/distort.html

;- Region: Includes
#include "include/Constants.csd"
#include "include/Opcodes.csd"

instr 1
    kMidiNote = p4
    //kMidiNote portk p4, (chnget:k("Glide_Rate"))  ; take the value of slider 2 and apply portamento
    //FLsetVal 1, kMidiNote, p4  ;set the value of slider 1 to kval


    //--------------------------------------------------------------------------------------------------------    
    ;- Region: FILTER/ENVELOPES
    //--------------------------------------------------------------------------------------------------------
    kEnvFilter madsr chnget:i("ENV_Attack"), chnget:i("ENV_Decay"), (chnget:i("ENV_Sustain") /10), chnget:i("ENV_Release") // Filter Envelope
    kAmpFilter madsr chnget:i("AMP_Attack"), chnget:i("AMP_Decay"), (chnget:i("AMP_Sustain") /10), chnget:i("AMP_Release") // Amplitude Envelope
    
    //--------------------------------------------------------------------------------------------------------    
    ;- Region: OSCILLATOR FREQUENCIES
    //--------------------------------------------------------------------------------------------------------
    // Set osc1 freq to midi freq / 2 then multiply this by 2^octave
    // this will allow us to cycle through octaves with rslider
    // e.g octave slider value can be 0,1,2,3 so if we bring 2^0 = 1, 2^1 = 2, 2^2 = 4....
    // this means when we multiply the midi note freq it will set it to the next octave
    // Divide initial freq by 2 allows us to have setting 1 on the slider be - 1 octave, setting 2 will be current octave and so on
    kFreq1 = ((kMidiNote * semitone(chnget:k("Fine_Tune")) * semitone(chnget:k("Pitch_Bend"))) / 2) * (2^chnget("OSC1_Octave"))

    // for the freq of osc2 we multiply osc1 freq by the semitone factor.
    kFreq2 = (kFreq1 * semitone(chnget:i("OSC2_Frequency"))) * (2^chnget("OSC2_Octave"))
    

    
    
    
    //--------------------------------------------------------------------------------------------------------    
    ;- Region: MODULATION
    //--------------------------------------------------------------------------------------------------------
    
    iModWheel = chnget:i("Mod_Wheel")
    iLFORate = chnget:i("LFO_Rate")
    iSource = chnget:i("Source")
     
    kPitchAmt = chnget:k("Pitch_AMT")
    kFilterAmt = chnget:k("Filter_AMT")
    kWaveAmt = chnget:k("Wave_AMT") 
    
    kOSC2OnlyBTN = chnget:k("OSC_2_Only")
    
    //Bypass LFO if source == 6
    iEnableLFO = ((iSource == 6) ? 0 : 1)
    
    //When lfo is enabled, MOD Values will == 1, so they won't have any effect, as we are multiplying by 1
    kFilterMOD = (((kFilterAmt > 0) && (iEnableLFO == 0)) ? (kFilterAmt * kEnvFilter * iModWheel) : 1)
    kWaveMOD = (((kWaveAmt > 0) && (iEnableLFO == 0)) ? (kWaveAmt * kEnvFilter * iModWheel) : 1)
    
    kFreq1 = (((kPitchAmt > 0) && (iEnableLFO == 0) && (kOSC2OnlyBTN == 0)) ? ((kPitchAmt * kEnvFilter * iModWheel * kFreq1) + kFreq1 ) : kFreq1)
    kFreq2 = (((kPitchAmt > 0) && (iEnableLFO == 0)) ? ((kPitchAmt * kEnvFilter * iModWheel * kFreq2) + kFreq2 ) : kFreq2)

    //--------------------------------------------------------------------------------------------------------
    ;- Region: MIXER
    //--------------------------------------------------------------------------------------------------------

    kOsc1Wave = chnget:k("OSC1_Wave") * kWaveMOD
    kOsc2Wave = chnget:k("OSC2_Wave") * kWaveMOD

    // SetWave opcode returns aVco based on the position of the osc knob
    aOSC1 SetWave kOsc1Wave, p5 * chnget:i("OSC1_Amp"), kFreq1
    aOSC2 SetWave kOsc2Wave, p5 * chnget:i("OSC2_Amp"), kFreq2

    aOSCSub vco2 p5 * chnget:i("SUB_Amp"), kFreq1/2, $WaveSquare
    aNoise pinker 
    aNoise = aNoise * (p5 * chnget:i("Noise_Amp"))

    aMix = aOSC1 + aOSC2 + aOSCSub + aNoise; // mix of osc1, osc2 the sub osc and noise

    //--------------------------------------------------------------------------------------------------------
    ;- Region: EG AMOUNT
    //--------------------------------------------------------------------------------------------------------
    //Notice that the EG AMOUNT knob is bipolar, meaning that its control value is positive when it’s turned
    //up and negative when it’s turned down. Turning it clockwise from center causes the envelope to raise
    //the cutoff frequency from the CUTOFF knob’s setting. Turning it counterclockwise from center causes
    //the envelope to lower the cutoff frequency from the CUTOFF knob’s setting.				

    //The depth of the envelope’s effect on the cutoff frequency also depends a lot on the CUTOFF setting.
    //If the setting is very high and you adjust the EG AMOUNT to raise it further, then the envelope will have
    //little effect. The lower the cutoff frequency, then the more the envelope will be able to modulate it. On
    //the other hand, if the setting is very low and you adjust the EG AMOUNT to lower it further by turning
    //the knob counterclockwise, again, the envelope will have little effect

    // Min and Max Value of the cutoff Frequency
    kMaxVal = 20000
    kMinVal = 20

    kCutoff = chnget("Cutoff") // Get cutoff freq range 20hz - 20000hz
    kEGAmount = chnget("EG") // Get EG Amount range -5 to +5

    kPercentage = 1 - (kCutoff - kMinVal) / (kMaxVal - kMinVal) // converts range to inverse percentage
				
    // Set Cutoff Freq based on eg amount and cutoff freq
    if (kEGAmount < 0) then
        kCutoff = kCutoff * (((kEGAmount / 10) + 1) + kPercentage / 2) //max cutoff and min eg = Ratio(0.5:1), min cutoff and min eg = (Ratio 1:1) 
    elseif (kEGAmount >= 0) then
        kCutoff = (kPercentage * (kEGAmount / 5) * kCutoff) + kCutoff //min cutoff and max eg = Ratio(2:1), max cutoff and max eg = (Ratio 1:1)
    endif

    //--------------------------------------------------------------------------------------------------------    
    ;- Region: KB AMOUNT
    //--------------------------------------------------------------------------------------------------------
    //KB AMOUNT: Use this knob to specify how much the filter cutoff tracks the keyboard; that is, how
    //much the keyboard pitch affects the filter’s lowpass frequency. With KB AMOUNT turned up halfway,
    //the filter cutoff will follow the keyboard pitch at a 1:1 ratio centered around middle C (MIDI note 60).
    //KB AMOUNT at maximum sets a 2:1 ratio for filter keyboard tracking
    
    kKBAmount = chnget("KB")
    // if KB amount is up full 10/5 = 2, So multiply the midi freq by 2 and add it to the current cutoff = 2:1 ratio
    kCutoff += (p4 * (kKBAmount/5))


    //--------------------------------------------------------------------------------------------------------    
    ;- Region: LFO (IF Enabled)
    //--------------------------------------------------------------------------------------------------------  
    if (iEnableLFO == 1) then
        kLfo lfo iModWheel, iLFORate, iSource
        kCutoff = (kCutoff * kLfo)
    endif

    aLadder moogladder aMix , abs(kCutoff * kEnvFilter * kFilterMOD ), chnget("Resonance")
    
    aSig = aLadder * kAmpFilter * chnget:i("Master")

    //--------------------------------------------------------------------------------------------------------    
    ;- Region: Multidrive
    //--------------------------------------------------------------------------------------------------------
    //MULTIDRIVE: MultiDrive is the Sub Phatty’s distortion processor, offering effects ranging from
    //asymmetrical, tube-like warmth to aggressive hard clipping, with a smooth continuous transition in
    //between. The MULTIDRIVE knob controls how hard you drive the OTA and FET stages, which are located
    //between the filter and the amplifier in the signal path. The higher the setting, then the more aggressive
    //the clipping effect
    
    kDist = chnget("Multidrive")
    aOut distort aSig, kDist, gifn
    
    outs aOut, aOut

endin

</CsInstruments>
<CsScore>

//f 1 0 1024 7 0 chnget("ENV_Attack") p5 chnget("ENV_Decay") 0.5 chnget("ENV_Sustain") 0.5 chnget("ENV_Release") 0

f0 z
</CsScore>
</CsoundSynthesizer>