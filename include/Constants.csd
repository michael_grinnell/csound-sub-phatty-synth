//Set Waveform Types, Sub-phatty has 4 - triangle, sawtooth, square, and pulse.

#define WaveTriangle #12#
#define WaveSawtooth #0#
#define WaveSquare #10#
#define WavePulse #6#
