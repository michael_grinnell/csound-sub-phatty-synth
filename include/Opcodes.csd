        // Mixes Two VCOs Together with a ratio based on kWavemix
        // if wavemix is 1 it will return full vco1
        // if wavemix is 0 it will return full vco2
        // 0.5 will return even mix
        
        opcode Mixer, a, aak

            aVco2, aVco1, kWaveMix     xin          
            aSum = (aVco1* kWaveMix)+aVco2*(1-kWaveMix)        
            xout aSum  
                        
        endop
        
        // Returns Waveform mix based on slider value
        opcode SetWave, a, kik
              
            kSliderValue, iAmplitude, kFreq xin        
                       
            // KDiff will always be in range(0,1)
            kDiff  =  kSliderValue
        
            // Different waves will be set based on the slider position
            // kDiff will be the amout the slider is between the waveset
            // e.g sliderVlaue = 1.5, kDiff = 0.5, wave will be 50% triangle and 50% sawtooth
            if (kSliderValue <= 1) then  
                iWave1 = $WaveTriangle
                iWave2 = $WaveSawtooth
  
            elseif (kSliderValue <= 2) then
                kDiff -= 1
                iWave1 = $WaveSawtooth
                iWave2 = $WaveSquare
  
            elseif (kSliderValue <= 3) then
                kDiff -= 2
                iWave1 = $WaveSquare
                iWave2 = $WavePulse
                
            endif
           
           // create a Vco for each wave needed       
           aVco1 vco2 iAmplitude, kFreq, iWave1
           aVco2 vco2 iAmplitude, kFreq, iWave2 
           
           // Mix Vcos together
           aSum Mixer aVco1,aVco2, kDiff
            
           xout aSum         
        endop
